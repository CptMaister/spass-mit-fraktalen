# Kochsche Schneeflocke

Informationen zur Kochschen Schneeflocke gibt es [Hier](https://de.wikipedia.org/wiki/Koch-Kurve).   

![Kochsche Schneeflocke 1. Iteration (Dreieck)](/images/koch-png/koch-flake-1-alt.png "Koch Flocke It. 1") ![Kochsche Schneeflocke 2. Iteration (Dreieck mit Zacken)](/images/koch-png/koch-flake-2.png "Koch Flocke It. 2") ![Kochsche Schneeflocke 3. Iteration](/images/koch-png/koch-flake-3.png "Koch Flocke It. 3") ![Kochsche Schneeflocke 4. Iteration](/images/koch-png/koch-flake-4.png "Koch Flocke It. 4") ![Kochsche Schneeflocke 5. Iteration](/images/koch-png/koch-flake-5.png "Koch Flocke It. 5")   

Um die Kochsche Schneeflocke zu malen, programmieren wir 2 Manöver: Das erste Manöver malt uns ein Drittel der Kochschenschneeflocke, also genau eine der 3 Seiten. Das nennen wir zum Beispiel `kochKurve`.   
Das zweite Manöver malt unsere 3 Seiten, indem es unser erstes Manöver 3 mal aufruft, aber dazwischen unsere Schildkröte im passenden Winkel dreht. Das nennen wir `kochFlocke`.   
Nicht vergessen: Alle unsere Manöver schreiben wir in `shapes.py` rein.

Ganz wichtig: Immer, wenn wir ein neues Manöver in `shapes.py` programmiert haben, wollen wir dieses ausprobieren. Das hilft uns zu sehen, ob wir Fehler gemacht haben und ob das gewünschte Ergebnis herauskommt. Ab hier wird nicht mehr extra daran erinnert. Trotzdem werden wir jedes neu definierte Manöver in unserer `main.py` testen!

Als erstes wollen wir also das Manöver `kochKurve` programmieren, das unsere Schildkröte und die Länge der Schneeflockenseite als Werte annimmt. Da ein Wegstück unserer Kurve -- zunächst mit nur einer Zacke -- dann ein Drittel der Kurve ausmacht, sollten wir nicht `laenge` weit vorwärts laufen bei jedem Abschnitt, sondern `laenge / 3` weit, also ein Drittel von `laenge`.

So könnte dein Code aussehen:   
![Kochflocke Drittel 2. Iteration (Linie mit Zacke)](/images/koch-png/koch-curve-2.png "Koch Kurve It. 2")
```python
def kochKurve(turtle, laenge):
  turtle.forward(laenge / 3)
  turtle.left(60)
  turtle.forward(laenge / 3)
  turtle.right(120)
  turtle.forward(laenge / 3)
  turtle.left(60)
  turtle.forward(laenge / 3)
```

Jetzt schreiben wir unser Manöver `kochFlocke`, mit Schleife, damit wir nicht 3 mal das gleiche schreiben müssen:   
![Kochsche Schneeflocke 2. Iteration (Dreieck mit Zacken)](/images/koch-png/koch-flake-2.png "Koch Flocke It. 2")
```python
def kochFlocke(turtle, laenge):
  for i in range(3):
    kochKurve(turtle, laenge)
    turtle.right(120)
```

Um jetzt noch mehr Zacken zu haben, rufen wir einfach das Manöver `kochKurve` in sich selbst an der Stelle auf, an der wir sonst einfach vorwärts gegangen wären. Das sähe dann so aus (**Achtung**, funktioniert so noch nicht ganz! Siehe dafür den nächsten Text und Code):
```python
def kochKurve(turtle, laenge):
  kochKurve(turtle, laenge / 3)
  turtle.left(60)
  
  kochKurve(turtle, laenge / 3)
  turtle.right(120)

  kochKurve(turtle, laenge / 3)
  turtle.left(60)
  
  kochKurve(turtle, laenge / 3)
```
![Fehlermeldung bei trinket bei nicht endender Rekursion](/images/other/koch-rekursionsfehler.png "trinket Rekursionsfehler")


Damit wir das aber nicht bis in alle Ewigkeit machen, bestimmen wir ein unteres Limit `limitmin`. Müssen wir auch, sonst kriegen wir diesen "Maximum call stack size exceeded"-Fehler nicht weg. Mit `limitmin` bestimmen wir, dass die kürzeste Streckenlänge mindestens so lang ist, wie der übergebene Wert für `limitmin`, indem wir zuerst eine entsprechende Abfrage machen. Ist dieses Limit erreicht, läuft die Schildkröte einfach vorwärts, anstatt nochmal `kochKurve` aufzurufen:
```python
def kochKurve(turtle, laenge, limitmin):
  # Wir prüfen zuerst, ob laenge bereits den Wert von limitmin unterschritten hat.
  if laenge < limitmin:
    turtle.forward(laenge)
  else:
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.right(120)
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, limitmin)
```

Und schon sind wir fertig und können durch Änderungen am Wert von `limitmin` beeinflussen, wie viele Zacken die Flocke haben wird. Wir sollten nicht vergessen, `limitmin` auch noch beim Manöver `kochFlocke` zu übergeben:
```python
def kochFlocke(turtle, laenge, limitmin):
  for i in range(3):
    kochKurve(turtle, laenge, limitmin)
    turtle.right(120)
```


## Unser Beispielcode sieht am Ende zum Beispiel so aus:

`main.py`:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(25)

inge.penup()
inge.goto(-100, 100)

inge.pendown()
kochFlocke(inge, 200, 15)
inge.penup()

inge.goto(0, -100)
inge.write("Meine Kochsche Schneeflocke :-)", None, "center", "15pt bold")
inge.goto(0, -115)
inge.seth(0)
```


`shapes.py`:
```python
import turtle

def kochFlocke(turtle, laenge, limitmin):
  for i in range(3):
    kochKurve(turtle, laenge, limitmin)
    turtle.right(120)

def kochKurve(turtle, laenge, limitmin):
  if laenge < limitmin:
    turtle.forward(laenge)
  else:
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.right(120)
    kochKurve(turtle, laenge / 3, limitmin)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, limitmin)
```


## So könnte unsere `shapes.py` aussehen mit Iterationsanzahl `schritte` als Eingabeparameter, statt mindester Strichlänge `limitmin`

```python
import turtle

def kochFlocke(turtle, laenge, schritte):
  for i in range(3):
    kochKurve(turtle, laenge, schritte)
    turtle.right(120)

def kochKurve(turtle, laenge, schritte):
  if schritte < 1:
    turtle.forward(laenge)
  else:
    kochKurve(turtle, laenge / 3, schritte - 1)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, schritte - 1)
    turtle.right(120)
    kochKurve(turtle, laenge / 3, schritte - 1)
    turtle.left(60)
    kochKurve(turtle, laenge / 3, schritte - 1)
```
Fast so wie vorher! Geändert wurde tatsächlich nur genau jede Stelle, an der vorher das Wort `limitmin` stand und die Bedingung bei der *if*-Abfrage. Der Rest des Codes ist wie zuvor. In der *if*-Abfrage wird jetzt gefragt, ob wir die `schritte` schon bis zur 0, also unter 1, runtergezählt haben. Und bei jeder nächsten Iteration wird der Wert von `schritte` bei der Übergabe in `kochKurve` um 1 gesenkt.


# Bonusaufgaben

Du hast die Anleitung zur Kochschen Schneeflocke verstanden oder die ganze Schneeflocke sogar erfolgreich selbst programmiert? Dann versuche dich an diesen Bonusaufgaben! Ändere deinen Code so, dass du solche oder ähnliche Flocken erzeugen kannst. Zur Hilfe wird die Idee hinter jedem Bild beschrieben.   

Falls du selbst auf interessante Variationen der Kochschen Schneeflocke kommst, kannst du mir sehr gerne dein Ergebnis unter alex.mai@posteo.net zuschicken. Vielleicht schafft es deine Variation ja in diese Bonusaufgabensammlung! Und falls du meine Lösungsvorschläge zu den Bonusaufgaben sehen magst, zum Beispiel um den Fehler in deinem Programm zu finden, kannst du mich auch unter alex.mai@posteo.net anschreiben.


### Flocke farbig und ausgefüllt ("Farb-Flocke")
Die Flocke soll mit einer gewählten Farbe ausgefüllt, der Rand mit einer anderen Farbe gemalt werden.   
![Kochsche Schneeflocke dunkelblauer Rand und hellblaue Füllung](/images/koch-png/koch-farbig-ausgefuellt.png "Koch farbig ausgefüllt")


### Flocke mit vier Seiten und Quadraten statt Zacken ("Quadrat-Flocke")
In der ersten Iteration soll unsere Flocke nun kein Dreieck mehr sein, sondern ein Quadrat. Außerdem ersetzen wir alle Zacken durch Quadrate.   
![Kochsche Schneeflocke mit Quadraten statt Dreiecken 1](/images/koch-png/koch-viereckig1it.png "Koch viereckig 1") ![Kochsche Schneeflocke mit Quadraten statt Dreiecken 2](/images/koch-png/koch-viereckig2it.png "Koch viereckig 2") ![Kochsche Schneeflocke mit Quadraten statt Dreiecken 3](/images/koch-png/koch-viereckig3it.png "Koch viereckig 3") ![Kochsche Schneeflocke mit Quadraten statt Dreiecken 4](/images/koch-png/koch-viereckig4it.png "Koch viereckig 4")


### Flocke aber nur die Zacken der letzten Iteration ("Geist-Flocke")
Wir wollen hier nur die kleinsten Zacken der letzten Iteration malen.   
![Kochsche Schneeflocke nur mit den kleinsten Zacken der 2. Iteration](/images/koch-png/koch-geist2it.png "Koch Geist 2 It.") ![Kochsche Schneeflocke nur mit den kleinsten Zacken der 3. Iteration](/images/koch-png/koch-geist3it.png "Koch Geist 3 It.") ![Kochsche Schneeflocke nur mit den kleinsten Zacken der 4. Iteration](/images/koch-png/koch-geist4it.png "Koch Geist 4 It.")


### Flocke mit verschiedenen Farben je nach Iteration ("Iterationen-Flocke")
Hier wollen wir die Zacken jeder Iteration verschieden einfärben. Also so, dass gleich große Zacken auch die gleiche Farbe haben. Hier sind zwei verschiedene Ansätze, jeweils mit verschiedener Reihenfolge, in der die Iterationen gemalt wurden. Tipp: Dafür ist die Geist-Flocke super praktisch!   
![Kochsche Schneeflocke mit verschiedenen Farben je nach Zackengröße bzw Iterationszahl](/images/koch-png/koch-iterationen.png "Koch iterationen") ![Kochsche Schneeflocke mit verschiedenen Farben je nach Zackengröße bzw Iterationszahl, andere Stapelung](/images/koch-png/koch-iterationen-rev.png "Koch iterationen")


### Flocke mit zufälligen Farben bei jedem Strich ("Regenbogen-Flocke")
Hier soll einfach jeder Strich in einer zufälligen Farbe gemalt werden. Eine Vorlage zur Erzeugung von zufälligen Farben für Inge findest du in der [einführenden Python Turtle Anleitung, ganz unten bei "Lustige Beispiele"](/Malen-in-Python-Turtle/README.md/#lustige-beispiele). Hier sind Varianten mit der normalen Flocke und mit jedem Strich aus jeder vorherigen Iteration.   
![Kochsche Schneeflocke mit zufälligen Farben bei jedem Strich, 2 It.](/images/koch-png/koch-bunt2it.png "Koch bunt 2") ![Kochsche Schneeflocke mit zufälligen Farben bei jedem Strich, 4 It.](/images/koch-png/koch-bunt4it.png "Koch bunt 4") ![Kochsche Schneeflocke mit zufälligen Farben bei jedem Strich, Iterationen](/images/koch-png/koch-iterationen-bunt.png "Koch Iterationen bunt rev") ![Kochsche Schneeflocke mit zufälligen Farben bei jedem Strich, andere Stapelung, Iterationen](/images/koch-png/koch-iterationen-bunt-rev.png "Koch Iterationen bunt")


### Flocke mit spitzeren Winkeln ("Winkel-Flocke")
Hier soll der Winkel der Zacken angepasst werden können. Das kann man schön als Eingabeparameter übergeben. Da das sehr schwer ist, kannst du zunächst versuchen, es für einen bestimmten Winkel zu programmieren. Hier sind ein paar der schönsten Flocken, die ich bei verschiedenen Winkeln und Iterationen gefunden habe.   
![Kochsche Schneeflocke mit verschiedenen spitzeren Winkeln und Iterationen](/images/koch-png/koch-spitzer-beste.png "Koch spitzer beste")
