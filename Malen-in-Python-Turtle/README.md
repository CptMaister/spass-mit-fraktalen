# Fraktale in Python mit [trinket.io](https://trinket.io)

Der Einfachheit halber machen wir im Folgenden alles in der Programmierumgebung von [trinket.io](https://trinket.io), benutzbar in jedem Browser, ohne vorher irgendwas installieren zu müssen.   
Entsprechend wird für alle Codes hier angenommen, dass man auf einem Canvas malt, der in beide x- und y-Richtungen 200 Pixel Platz hat. Die meisten Sachen werden sich nicht weiter als 150p rausbewegen.   

Du hast Verbesserungsvorschläge für diese oder andere Anleitungen hier? Dann schreib mir eine E-Mail! Unter alex.mai@posteo.net erreichst du mich :-)


# Turtle Grundlagen

## Einleitung
Wir brauchen eine Datei `main.py` und eine Datei `shapes.py`. Auf [trinket.io](https://trinket.io) sind beide Dateien schon angelegt und mit beispielhaftem Code befüllt. Wenn du gerne erst ausprobierst, und dann nachliest, schau dir das erstmal an und Spiele mit dem Code herum! :-)    

Wenn du entweder schon Erfahrung hast mit der Programmierung der Schildkröte, oder dich durch eigenes Rumprobieren vertraut gemacht hast, kannst du dich schon an der Programmierung der Fraktale versuchen. Ansonsten kannst du dich zuerst mit dieser Anleitung mit der Schildkröte anfreunden. Wie man die Fraktale Schritt für Schritt programmiert (vorher alleine versuchen, vielleicht schaffst du es ja!), siehst du, wenn du auf den entsprechenden Fraktalnamen klickst (jeweils ganz unten auch richtig krasse Bonusherausforderungen!):
+ [Kochsche Schneeflocke](Malen-in-Python-Turtle/Kochsche-Schneeflocke.md)
+ [Sierpinski-Dreieck](Malen-in-Python-Turtle/Sierpinski-Dreieck.md)
+ [Menger-Schwamm](Malen-in-Python-Turtle/Menger-Schwamm.md)
+ [Drachenkurve](Malen-in-Python-Turtle/Drachenkurve.md) (Sehr schwer!)

In `shapes.py` werden wir alle Manöver programmieren, die unsere Schildkröte öfter mal ausführen soll, wie etwa eine bestimmte Kurve malen.   
In `main.py` steht drinnen, was unsere Schildkröte machen soll. Sie kann dabei alle Manöver ausführen, die wir in `shapes.py` programmiert haben.   
Wir beschäftigen uns kurz mit den wesentlichen Inhalten unserer beiden Dateien, schauen uns dann die wichtigsten Aktionsmöglichkeiten unserer Schildkröte an und probieren dann einige Beispiele aus.


## Wie sieht die `main.py` Datei aus?

Zuerst einmal müssen wir die in Python verfügbaren, grundlegenden Schildkröten-Manöver importieren, sowie unserem Programm schonmal ankündigen, dass wir auch unsere selbstgemachten Manöver aus `shapes.py` brauchen werden.   

Danach müssen wir uns eine Schildkröte erstellen, mit Namen und Geschwindigkeit. Wir nennen sie Inge und lassen sie die Glücksgeschwindigkeit 7 haben. Das alles machen wir so:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(7)
```

Leerzeilen können wir dabei nach Belieben setzen, da unsere verwendete Programmiersprache Python diese ignoriert. Nachdem das geschafft ist, schreiben wir einfach in jede Zeile nacheinander, was unsere Schildkröte Inge tun soll. Das können selbstprogrammierte, aber auch normale Grundmanöver sein, wie sie in den nächsten Absätzen und in den Beispielen zu sehen sind.


## Wie sieht die `shapes.py` Datei aus?

Hier musst du dir wenige Gedanken machen. In der ersten Zeile müssen wir wieder alle vorprogammierten Befehle von `turtle` importieren. 

Danach können wir unsere eigenen Schildkrötenmanöver programmieren. Diese führen wir dann in unserer `main.py`-Datei aus. Als Beispiel bringe ich Inge hier bei, im Zick-Zack nach vorne zu laufen und dabei ihren Weg auf den Boden zu malen:

```python
import turtle

def zickzack(turtle, winkel, zackenlaenge, zackenzahl):
  turtle.pendown()
  turtle.left(90 - winkel/2)
  for i in range(zackenzahl):
    turtle.forward(zackenlaenge)
    turtle.right(180 - winkel)
    turtle.forward(zackenlaenge)
    turtle.left(180 - winkel)
  turtle.penup()
```

Um ein Manöver zu definieren (auch "Funktion" genannt), beginnen wir mit *def*.   

Dann unser Manövername.   

Dann in runden Klammern alle Eingabeparameter. Diese sind Vorgaben, in welcher Variation wir unser Manöver ausführen möchten. Bei guter Parameterwahl kann ein einziges Manöver viele tolle Sachen erzeugen! In diesem Beispiel können wir mit dieser einzelnen Funktion Zackenlinien mit beliebiger Zackenanzahl, Zackenwinkeln, und Zackenlänge malen.   

Nach den runden Klammern brauchen wir unbedingt einen Doppelpunkt! 

Danach kommen nacheinander alle Manöver, die ausgeführt werden sollen. Dabei kann man hier auch andere selbstprogrammierte Manöver aufrufen. oder sogar das Manöver, das man gerade programmiert! (Siehe [Rekursion](#schleifen-und-rekursionen))

Wichtig ist dabei anzumerken, dass alles, was wir hier programmieren, erst von Inge ausgeführt wird, wenn wir ihr das in unserer `main.py`-Datei sagen! Wie das dann aussieht, kannst du dir in den [Beispielen](#lustige-beispiele) ansehen. Du musst dafür nur den Namen deines Manövers hinschreiben und in den geöffneten Klammern alle geforderten Eingabeparameter übergeben, wie den Namen deiner Schildkröte und im Fall des Manövers zickzack 3 Zahlen, die jeweils für den Winkel der Zacken, die Zackenlänge und die Zackenanzahl stehen.

*Fun Fact*: im Fachjargon bezeichnet man das, was wir hier als Manöver bezeichnen, normalerweise als Funktion.


## Was kann unsere Schildkröte Inge schon, bevor wir ihr neue Tricks beibringen?
In der [englischsprachigen Dokumentation](https://docs.python.org/2/library/turtle.html) kann man sich alle Befehle durchlesen, die Inge schon kann. Im Folgenden findest du aber alles, was wir zum Fraktale Malen brauchen.

### Das wichtigste zuerst

+ `inge.penup()`   
Hiermit hebt Inge ihren Stift und hinterlässt so beim Umherlaufen keine Spur.

+ `inge.pendown()`   
Hiermit setzt Inge ihren Stift wieder ab und hinterlässt beim Umherlaufen eine Spur. Wenn du willst, dass Inge etwas für dich malt, musst du sie nur mit abgesetztem Stift umherlaufen lassen.

+ `inge.goto(-50, 31.415)`   
Inge bewegt sich an die Stelle (-50, 31.415), wobei -50 hier die x-Koordinate ist und 31.415 die y-Koordinate. Kommazahlen werden hier, wie im englischsprachigen Raum üblich, mit einem Punkt statt einem Komma geschrieben. Die Mitte der Zeichenfläche befindet sich bei (0, 0). Die x-Koordinate gibt an, wie weit rechts man sich von der Mitte befindet. Die y-Koordinate, wie weit oben.

+ `inge.setheading(45)`   
Hiermit weiß Inge, in welche Richtung sie schauen soll. Die Angabe ist hier in Grad und entspricht der Richtung auf einem normalen Koordinatensystem. Hier ein paar wichtige Gradzahlen und die entsprechende Richtung:   
0 - rechts   
90 - oben   
180 - links   
270 - unten   
Eine kürzere Schreibweise ist `inge.seth(45)`

+ `inge.left(90)`   
Inge dreht sich um 90 Grad nach links.

+ `inge.right(120)`   
Inge dreht sich um 120 Grad nach rechts.

+ `inge.forward(42)`   
Inge geht 42 Pixel vorwärts

+ Du kannst Kommentare in deinen Code schreiben, um dir selbst und anderen Leuten zu erklären, was an dieser Stelle programmiert wurde oder was du dir dabei gedacht hast. Das ist super wichtig, sobald dein Code länger als 10 Zeilen ist, da du sonst nicht mehr weißt, was du programmiert hast, wenn du in ein paar Tagen wieder draufschaust! Zum kommentieren beginne eine Zeile mit dem Hash- beziehungsweise dem Rauten-Symbol #. Diese Zeile wird von deinem Programm ignoriert und ist nur dazu da, um von Menschen gelesen zu werden.

### Spielereien

+ `inge.speed(42)`   
Geschwindigkeit von Inge auf 42 ändern.

+ `inge.color("red")`   
Farbe von Inge, und damit auch die Farbe der Linie, die Inge hinterlässt, auf rot ändern.   
*siehe Katalog für *[andere Farben](#katalog)*

+ `inge.fillcollor("blue")`   
Farbe, mit der Formen ausgefüllt werden. Damit eine Form ausgefüllt wird, sollte davor `inge.begin_fill()` und danach `inge.end_fill()` stehen.

+ `inge.end_fill()`   
Damit hört Inge auf, unsere Formen mit Farbe zu füllen.

+ `inge.write("Mathe macht Spaß!", None, "center", "16pt bold")`
Damit schreibt Inge den Text "Mathe macht Spaß!" an der Stelle, an der sie steht. Dabei ist die Mitte des Texts genau da, wo Inge gerade steht und der Text hat die Textgröße 16pt.

+ `inge.hideturtle()`   
Lässt Inge verschwinden.

+ `inge.showturtle()`   
Lässt Inge wieder auftauchen.


## Allgemeine Python-Programmiertricks, um richtig krasse Sachen zu malen
Im Folgenden wollen wir lernen, wie wir dem Computer erklären, was wir machen wollen, und wie oft wir das machen wollen. Dadurch wird unser Code nicht nur **schöner und leichter zu bearbeiten**, sondern auch **viel kürzer und weniger fehlerhaft**.

Wir haben jetzt schon gelernt, wie wir Inge einzelne Manöver ausführen lassen können, aber unser Ziel ist es ja hier, dass Inge ein Fraktal malt. Fraktale, aber auch viele andere Dinge im Leben, wiederholen sich oft. Und da wir nicht tausend mal "inge.forward(10)" eingeben wollen, um jeden einzelnen Strich unserer Kochschen Schneeflocke zu malen, wollen wir das tun, wofür Computer gebaut wurden: Die Computer die Wiederholung für uns machen lassen. 

Wir wollen uns dabei, vor allem mit dem Ziel, Fraktale zu malen, zwei wichtige Dinge anschauen: **Schleifen** und **Rekursionen**.


### Schleifen und Rekursionen

Wir schauen uns jeweils ein Beispiel an und erklären, was im Beispiel passiert. An dem Beispiel kannst du dann rumspielen, oder im Internet nach weiteren Beispielen recherchieren, um ein richtiger Schleifen- und Rekursionsmeister zu werden! :-)


#### *for*-Schleifen
```python
strecke = 1
for zaehler in range(200):
  inge.forward(strecke)
  inge.left(91)
  strecke = strecke + 1
```

Zuerst erstellen wir hier die Variable `strecke` mit dem Anfangswert 1. Das brauchen wir nicht konkret für Schleifen, aber in der folgenden Schleife möchte ich diese Variable verwenden.   

Danach kommt auch schon der wichtigste Teil der Schleife: *for* gefolgt von einer Variable, die von 1 bis 200 durchgezählt wird. Sie beginnt bei 1 und geht bis 200, weil *range(200)* das für uns macht. *range* ist deswegen super praktisch für *for*-Schleifen, weil wir so das, was in unserer Schleife passieren soll, genau 200 mal machen können. Unsere Variable `zaehler` wird nämlich nach jedem Schleifendurchlauf genau um 1 erhöht, bis wir 200 erreicht haben.

Nach einem Doppelpunkt folgt in den nächsten Zeilen das, was in unserem Fall 200 mal passieren soll. Das alles müssen wir aber gleich weit nach rechts einrücken, zum Beispiel durch ein Leerzeichen zu Beginn der Zeile. Denn alles, was genau so weit links steht, wie *for*, gehört nicht mehr zur Schleife. Das wird also erst nach der Schleife ausgeführt!

In unserer Schleife geht Inge immer `strecke` weit vorwärts und dreht sich dann um 91 Grad nach links. Also fast im rechten Winkel, nur ein bisschen weiter noch. Danach wird die Variable `strecke` um 1 erhöht, wodurch Inge beim nächsten Schleifendurchlauf 1 weiter laufen wird, als beim letzten Mal.

*Fun Fact*: Wir können obigen Code auch folgendermaßen schreiben, mit dem gleichen Ergebnis!
```python
for i in range(200):
  inge.forward(i)
  inge.left(91)
```

#### *while*-Schleifen
Diese braucht man, um etwas so lange auszuführen, wie eine Aussage war ist. Das heißt, die Schleife bricht ab, sobald die Aussage nicht mehr wahr ist. Ein lustiges Thema, brauchen wir aber nicht für Fraktale. Trotzdem schnell ein Beispiel:
```python
strecke = 1
maxstrecke = 150
while strecke < maxstrecke:
  inge.forward(strecke)
  inge.right(60)
  strecke = strecke + 3
```

Hier starten wir mit einer Variable `strecke` mit dem Wert 1 und einer Variable `maxstrecke` mit dem Wert 150. In der Schleifenvorschrift schreiben wir *while* gefolgt von einer Aussage, die entweder wahr, oder falsch ist. Solange `strecke` kleiner als `maxstrecke` ist, wird also die Schleife weitergehen. Da aber am Ende jedes Schleifendurchlaufs der Wert von `strecke` um 3 erhöht wird, stimmt diese Aussage nicht mehr und die Schleife bricht ab.

Probiere den Code mal aus und schau, was dabei rauskommt!


#### Rekursion
Schau dir dafür am besten direkt an, wie das bei der Programmierung der Kochschen Schneeflocke, des Sierpinski-Dreiecks oder des Menger-Schwamms gemacht wird. Rekursion versteht man am besten durchs Ausprobieren!


## Katalog
Hier findest du mögliche Eingaben für oben beispielhaft verwendete Werte, wie mögliche Farben in Python Turtle, mögliche Richtungsangaben und alles weitere.   

#### Farben: 
"black", "green", "blue", "yellow", "red", und viele andere englische Farbbezeichnungen funktionieren. [Hier](https://trinket.io/docs/colors) siehst du alle Farben, die bei Python Turtle auf [trinket.io](https://trinket.io) funktionieren. Wenn du auf eine Farbe draufklickst, siehst du unter anderem ihren Namen und ihren RGB-Wert.
Du kannst auch direkt die [RGB-Werte](https://de.wikipedia.org/wiki/RGB-Farbraum) deiner gewünschten Farbe eingeben, wie zum Beispiel `inge.color((65, 105, 225))` für ein sattes RoyalBlue. In allen Fällen zum Beispiel, in denen du ein kräftiges Rot haben willst, kannst du statt `"red"` einfach `(255, 0, 0)` eingeben.   
Eine schöne Tabelle mit den RGB-Werten zu ganz vielen Farben gibt es [Hier](http://www.farb-tabelle.de/de/farbtabelle.htm).

#### Grad für Blick- und Drehrichtung:
`inge.right(90)` dreht Inge genau im rechten Winkel nach rechts. Wie wir wissen, steht 90 Grad nämlich für den rechten Winkel. Eine ganze Drehung entspricht 360 Grad. Entsprechend dreht sich Inge mit `inge.left(60)` genau ein Drittel von der Drehung nach links, die Inge bräuchte, um sich umzudrehen.

`inge.seth(0)` lässt Inge nach rechts schauen. 0 Grad ist unter Mathematikern Osten bzw. rechts. 90 Grad ist oben, 180 Grad ist links, 270 Grad ist unten und 360 Grad ist wieder rechts und das gleiche wie 0 Grad - zumindest wenn man von Grad als Rotationsangabe spricht! Entsprechend schaut Inge mit `inge.seth(135)` nach oben links.



## Lustige Beispiele:

So kann deine `main.py` aussehen, wenn du das obige Beispielmanöver [zickzack](#einleitung) ausprobieren möchtest:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(10)

inge.penup()
inge.goto(-150, 0)
inge.seth(0)

zickzack(inge, 95, 40, 5)
```

Das hier kannst du direkt in deiner `main.py` probieren, oder es als Funktion verpacken:
```python
from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(50)

for i in range(200):
  inge.forward(i)
  inge.left(91)
```

Als Funktion sähe das zum Beispiel so aus:
```python
def quadratSpirale(turtle)
  for i in range(200):
    turtle.forward(i)
    turtle.left(91)
```

Hiermit kannst du die Farbe von Inge zufällig ändern. In einer Schleife entfaltet das einen super Effekt, da Inge dann bei jedem Schleifendurchlauf eine andere zufällige Farbe hat. Du musst dafür aber ganz oben noch `random` importieren. Dafür schreibst du am einfachsten direkt in die erste Zeile `import random`:
```python
a = random.randint(1, 255)
b = random.randint(1, 255)
c = random.randint(1, 255)
inge.color((a, b, c))
```
