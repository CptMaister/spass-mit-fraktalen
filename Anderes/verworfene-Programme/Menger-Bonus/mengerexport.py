from turtle import *
import turtle
import random

def mengerWuerfel(turtle, laenge, winkel, schieb, it):
  mengerWinkel(turtle, laenge, laenge, it, 90)

  saveX = turtle.xcor()
  saveY = turtle.ycor()
  
  turtle.left(winkel)
  turtle.penup()
  turtle.forward(schieb)
  turtle.right(winkel)
  mengerWinkel(turtle, schieb, laenge, it, 180 - winkel)
  
  turtle.penup()
  turtle.goto(saveX, saveY)
  turtle.forward(laenge)
  turtle.right(90)
  turtle.forward(laenge)
  turtle.right(180)
  mengerWinkel(turtle, schieb, laenge, it, 90 - winkel)

def mengerWinkel(turtle, hoehe, breite, schritte, winkel):
  turtle.pendown()
  
  if schritte < 1:
    turtle.begin_fill()
    parallelogramm(turtle, hoehe, breite, winkel)
    turtle.end_fill()

  else:
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.forward(breite/3)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.forward(breite/3)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.right(winkel)
    turtle.forward(hoehe/3)
    turtle.left(winkel)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.right(winkel)
    turtle.forward(hoehe/3)
    turtle.left(winkel)
  
    #unten rechts
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.backward(breite/3)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.backward(breite/3)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.left(180 - winkel)
    turtle.forward(hoehe/3)
    turtle.right(180 - winkel)
  
    mengerWinkel(turtle, hoehe/3, breite/3, schritte-1, winkel)
    turtle.left(180 - winkel)
    turtle.forward(hoehe/3)
    turtle.right(180 - winkel)
  
  turtle.penup()


def parallelogramm(turtle, hoehe, breite, winkel):
  turtle.forward(breite)
  turtle.right(winkel)
  
  turtle.forward(hoehe)
  turtle.right(180 - winkel)
  
  turtle.forward(breite)
  turtle.right(winkel)
  
  turtle.forward(hoehe)
  turtle.right(180 - winkel)


inge = Turtle()
inge.speed(0)

inge.penup()
inge.goto(-100, -50)
inge.fillcolor("magenta")

mengerWuerfel(inge, 200, 45, 100, 3)

inge.hideturtle()

canv = inge.getscreen().getcanvas() 
canv.postscript(file="/home/bimmler/Desktop/turtleexports/menger-wuerfel3it.ps") 
