Jede Dreiecksfläche und -seite in einer anderen zufälligen Farbe befüllen.




main.py:

from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(70)

inge.penup()
inge.goto(-100, -80)

sierpinski(inge, 200, 3)

inge.hideturtle()




shapes.py:

import turtle
import random

def sierpinski(turtle, laenge, schritte):
  turtle.pendown()
  
  if schritte < 1:
    turtle.begin_fill()
    dreieck(turtle, laenge)
    turtle.end_fill()
  
  else:
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    sierpinski(turtle, laenge/2, schritte-1)
    turtle.forward(laenge/2)
    turtle.left(120)
    turtle.forward(laenge/2)
  
    turtle.forward(laenge/2)
    turtle.left(120)
  
  turtle.penup()


def dreieck(turtle, laenge):
  a = random.randint(1, 255)
  b = random.randint(1, 255)
  c = random.randint(1, 255)
  turtle.fillcolor((a, b, c))

  for i in range(3):
    a = random.randint(1, 255)
    b = random.randint(1, 255)
    c = random.randint(1, 255)
    turtle.color((a, b, c))
    turtle.forward(laenge)
    turtle.left(120)

