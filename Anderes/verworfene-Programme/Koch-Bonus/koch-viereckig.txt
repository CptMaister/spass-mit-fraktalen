Sehr interessant, wir ersetzen die Zacke durch ein Quadrat. Dadurch sieht unsere Schneeflocke ab 4 Iterationen völlig verrückt aus, und zwar wie ein quadratischer Rahmen mit größer werdenden Bäumen am Rand.




main.py:

from turtle import *
from shapes import *

inge = Turtle()
inge.shape("turtle")
inge.speed(25)

inge.penup()
inge.goto(-100, 100)

kochFlocke(inge, 200, 3)






shapes.py:

import turtle

def kochFlocke(turtle, laenge, schritte):
  turtle.pendown()
  for i in range(4):
    kochQuadrat(turtle, laenge, schritte)
    turtle.right(90)
  turtle.penup()


def kochKurve(turtle, laenge, schritte):
  kochQuadrat(turtle, laenge, schritte)
  turtle.left(90)
  kochQuadrat(turtle, laenge, schritte)
  turtle.right(90)
  kochQuadrat(turtle, laenge, schritte)
  turtle.right(90)
  kochQuadrat(turtle, laenge, schritte)
  turtle.left(90)
  kochQuadrat(turtle, laenge, schritte)


def kochQuadrat(turtle, laenge, schritte):
  if schritte > 1:
    kochKurve(turtle, laenge / 3, schritte-1)
  else:
    turtle.forward(laenge)

