## Notizen zum Einsatz von Bildern in Markdown-files

Trotz Anleitung vom [offiziellen Gitlab Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/#images), funktionieren folgende Sachen, zumindest bei meinen md-files in "Malen mit Python Turtle", nicht:
 - `{: .shadow}` am Ende vom Bild um einen Schatten hinzuzufügen
 - `*My Caption*` zum Hinzufügen von Bildunterschriften
 - svg-Dateien

 Und Links sollte man, anders als im Guide, aber genau wie bei Texten mit Links, in runde Klammern setzen, den ganzen Bildausdruck einfach in eckige Klammern.